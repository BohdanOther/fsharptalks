﻿module PatternMatching

open System

/// A record for a person's first and last name
type Person = {
    First : string
    Last  : string
}

/// A Discriminated Union of 3 different kinds of employees
type Employee =
    | Engineer of engineer: Person
    | Manager of manager: Person * reports: List<Employee>
    | Executive of executive: Person * reports: List<Employee> * assistant: Employee

/// Count everyone underneath the employee in the management hierarchy,
/// including the employee.
let rec countReports(emp : Employee) =
    1 + match emp with
        | Engineer(id) ->
            0
        | Manager(id, reports) ->
            reports |> List.sumBy countReports
        | Executive(id, reports, assistant) ->
            (reports |> List.sumBy countReports) + countReports assistant


/// Find all managers/executives named "Dave" who do not have any reports.
/// This uses the 'function' shorthand to as a lambda expression.
let rec findDaveWithOpenPosition(emps : List<Employee>) =
    emps
    |> List.filter(function
                    | Manager({First = "Dave"}, []) -> true // [] matches an empty list.
                    | Executive({First = "Dave"}, [], _) -> true
                    | _ -> false) // '_' is a wildcard pattern that matches anything.
                                    // This handles the "or else" case.

/// You can also use the shorthand function construct for pattern matching, 
/// which is useful when you're writing functions which make use of Partial Application.
let private parseHelper f = f >> function
    | (true, item) -> Some item
    | (false, _) -> None

let parseDateTimeOffset = parseHelper DateTimeOffset.TryParse

let result = parseDateTimeOffset "1970-01-01"
match result with
| Some dto -> printfn "It parsed!"
| None -> printfn "It didn't parse!"