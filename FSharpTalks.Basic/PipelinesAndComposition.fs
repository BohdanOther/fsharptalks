﻿module PipelinesAndComposition

/// Squares a value.
let square x = x * x

/// Adds 1 to a value.
let addOne x = x + 1

/// Tests if an integer value is odd via modulo.
let isOdd x = x % 2 <> 0

/// A list of 5 numbers.  More on lists later.
let numbers = [ 1; 2; 3; 4; 5 ]



/// 1. Given a list of integers, it filters out the even numbers,
/// squares the resulting odds, and adds 1 to the squared odds.
let squareOddValuesAndAddOne values = 
    let odds = List.filter isOdd values
    let squares = List.map square odds
    let result = List.map addOne squares
    result

printfn "%A -> %A" numbers (squareOddValuesAndAddOne numbers)
    


/// 2. A shorter way to write 'squareOddValuesAndAddOne' is to nest each
/// sub-result into the function calls themselves.
///
/// This makes the function much shorter, but it's difficult to see the
/// order in which the data is processed.
let squareOddValuesAndAddOneNested values = 
    List.map addOne (List.map square (List.filter isOdd values))

printfn "%A -> %A" numbers (squareOddValuesAndAddOneNested numbers)



/// 3. A preferred way to write 'squareOddValuesAndAddOne' is to use F# pipe operators.
/// This allows you to avoid creating intermediate results, but is much more readable
/// than nesting function calls like 'squareOddValuesAndAddOneNested'
let squareOddValuesAndAddOnePipeline values =
    values
    |> List.filter isOdd
    |> List.map square
    |> List.map addOne

printfn "%A -> %A" numbers (squareOddValuesAndAddOnePipeline numbers)

/// 4. You can shorten 'squareOddValuesAndAddOnePipeline' by moving the second `List.map` call
/// into the first, using a Lambda Function.
///
/// Note that pipelines are also being used inside the lambda function.  F# pipe operators
/// can be used for single values as well.  This makes them very powerful for processing data.
let squareOddValuesAndAddOneShorterPipeline values =
    values
    |> List.filter isOdd
    |> List.map(fun x -> x |> square |> addOne)

printfn "%A -> %A" numbers (squareOddValuesAndAddOneShorterPipeline numbers)

/// 5. Lastly, you can eliminate the need to explicitly take 'values' in as a parameter by using '>>'
/// to compose the two core operations: filtering out even numbers, then squaring and adding one.
///
/// This is using what is known as function composition.  This is possible because functions in F#
/// use Partial Application and the input and output types of each data processing operation match
/// the signatures of the functions we're using.
let squareOddValuesAndAddOneComposition =
    List.filter isOdd >> List.map (square >> addOne)

printfn "%A -> %A" numbers (squareOddValuesAndAddOneComposition numbers)