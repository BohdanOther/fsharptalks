﻿module ImplementingInterfaces

/// This is a type that implements IDisposable.
type ReadFile() =

    let file = new System.IO.StreamReader("readme.txt")

    member this.ReadLine() = file.ReadLine()

    // This is the implementation of IDisposable members.
    interface System.IDisposable with
        member this.Dispose() = file.Close()


/// This is an object that implements IDisposable via an Object Expression
/// Unlike other languages such as C# or Java, a new type definition is not needed 
/// to implement an interface.
let interfaceImplementation =
    { new System.IDisposable with
        member this.Dispose() = printfn "disposed" }