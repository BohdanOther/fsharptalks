﻿/// A module is a grouping of F# code, such as values, types, and function values. 
/// Grouping code in modules helps keep related code together and helps avoid name conflicts in your program.
module Numbers

/// This is a sample integer.
let sampleInteger = 176

/// This is a sample floating point number.
let sampleDouble = 4.1

/// This is a sample integer with a type annotation
let sampleInteger3: int = 1