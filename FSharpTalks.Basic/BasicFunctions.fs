﻿module BasicFunctions

/// You use 'let' to define a function.
let sampleFunction1 x = x*x + 3

/// Apply the function, naming the function return result using 'let'. 
/// The variable type is inferred from the function return type.
let result1 = sampleFunction1 4573

// This line uses '%d' to print the result as an integer. This is type-safe.
// If 'result1' were not of type 'int', then the line would fail to compile.
printfn "The result of squaring the integer 4573 and adding 3 is %d" result1


/// When needed, annotate the type of a parameter name using '(argument:type)'.  Parentheses are required.
let sampleFunction2 (x:int) = 2*x*x - x/5 + 3


/// Conditionals use if/then/elid/elif/else.
/// F# uses whitespace indentation-aware syntax, similar to languages like Python.
let sampleFunction3 x = 
    if x < 100.0 then 
        2.0*x*x - x/5.0 + 3.0
    else 
        2.0*x*x + x/5.0 - 37.0

let result3 = sampleFunction3 (6.5 + 4.5)

// This line uses '%f' to print the result as a float.  As with '%d' above, this is type-safe.
printfn "The result of applying the 3rd sample function to (6.5 + 4.5) is %f" result3