﻿module Immutability

/// Binding a value to a name via 'let' makes it immutable.
let number = 2
//let number = 3 // error

/// A mutable binding.  This is required to be able to mutate the value of 'otherNumber'.
let mutable otherNumber = 2

printfn "'otherNumber' is %d" otherNumber



// When mutating a value, use '<-' to assign a new value.
//
// You could not use '=' here for this purpose since it is used for equality 
// or other contexts such as 'let' or 'module'
otherNumber <- otherNumber + 1

printfn "'otherNumber' changed to be %d" otherNumber